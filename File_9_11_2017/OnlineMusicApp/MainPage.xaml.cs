﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace OnlineMusicApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string url_search = "https://mp3.zing.vn/tim-kiem/bai-hat.html?q=";
        string match = "item-song";
        List<Song> songs;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //Load trang chu
        }

        async private void TxtKeywords_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            PrgLoading.IsActive = true;
            
            songs = new List<Song>();

            using (var client = new HttpClient())
            {
                    string url = url_search + TxtKeywords.QueryText;

                    var response = await client.GetBufferAsync(new Uri(url));
                    string html = Encoding.UTF8.GetString(response.ToArray());

                    int pos = 0;
                    while(html.IndexOf(match, pos) > 0)
                    {
                        pos = html.IndexOf(match, pos) + match.Length ;

                        pos = html.IndexOf("data-id", pos) + 9;
                        string id = html.Substring(pos, html.IndexOf("\"", pos) - pos);

                        pos = html.IndexOf("title", pos) + 7;
                        string title = html.Substring(pos, html.IndexOf("\"", pos) - pos);

                        pos = html.IndexOf("href", pos) + 6;
                        string href = html.Substring(pos, html.IndexOf("\"", pos) - pos);

                    songs.Add(new Song()
                        {
                            Id = id,
                            Title = title,
                            Href = "https://mp3.zing.vn" + href,
                            Info = "Chua biet",
                            Url = ""
                        });
                    } 
                }
            LstListPlay.ItemsSource = songs;
            PrgLoading.IsActive = false;
        }

        async private Task<string> GetSource(string href)
        {
            using (var client = new HttpClient())
            {
                string html = await client.GetStringAsync(new Uri(href));
                int pos = html.IndexOf("data-xml");
                if (pos > 0)
                {
                    pos += 10;
                    string url_xml = "https://mp3.zing.vn/xhr" + html.Substring(pos, html.IndexOf("\"", pos) - pos);
                    string json = await client.GetStringAsync(new Uri(url_xml));
                    JsonObject root = JsonObject.Parse(json);
                    JsonObject data = root.GetNamedValue("data").GetObject();
                    JsonObject source = data.GetNamedValue("source").GetObject();

                    return "http:" + source.GetNamedString("128");
                    
                }
            }
            return "";
        }

        async private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            string href = (sender as AppBarButton).CommandParameter.ToString();
            Song s = songs.Where(x => x.Href.Equals(href)).FirstOrDefault();
            
            if (s.Url.Equals(""))
            {
                s.Url = await GetSource(s.Href);
            }

            Player.Source = new Uri(s.Url);

        }

        async private void BtnDownload_Click(object sender, RoutedEventArgs e)
        {
            string href = (sender as AppBarButton).CommandParameter.ToString();
            Song s = songs.Where(x => x.Href.Equals(href)).FirstOrDefault();

            if (s.Url.Equals(""))
            {
                s.Url = await GetSource(s.Href);
            }

            StorageFile file = await KnownFolders.MusicLibrary.CreateFileAsync(s.Title + ".mp3", CreationCollisionOption.GenerateUniqueName);

            BackgroundDownloader downloader = new BackgroundDownloader();
            downloader.CreateDownload(new Uri(s.Url), file).StartAsync();


        }
    }
    
}
